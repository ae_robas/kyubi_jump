﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Logo : MonoBehaviour
{

    int time = 0;

    void Update()
    {
        time++;
        if (time >= 120)
        {
            SceneManager.LoadScene("Title");
        }
    }
}
